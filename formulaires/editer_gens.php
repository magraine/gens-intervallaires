<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/actions');
include_spip('inc/editer');

function formulaires_editer_gens_charger_dist($id_gens='new', $retour=''){
	$valeurs = formulaires_editer_objet_charger('gens', $id_gens, '', '', $retour, '');
	return $valeurs;
}

function formulaires_editer_gens_verifier_dist($id_gens='new', $retour=''){
	$erreurs = formulaires_editer_objet_verifier('gens', $id_gens, array('nom'));
	
	if (false === sql_intervallaire::est_deplacable_sur_parent('spip_gens', $id_gens, _request('id_parent'))) {
		$erreurs['id_parent'] = _T('intervallaire:erreur_parent_insolvable');
	}
	
	return $erreurs;
}


function formulaires_editer_gens_traiter_dist($id_gens='new', $retour=''){
	$id_parent = intval(_request('id_parent'));
	
	// plusieurs cas :
	// j'existe et mon parent a change...
	// j'existe et mon parent n'a pas change...
	// je n'existe pas encore...
	if (intval($id_gens)) {
		$mon_parent = sql_getfetsel('id_parent', 'spip_gens', 'id_gens='.sql_quote($id_gens));
		// on modifie l'arborescence uniquement si le parent change.
		if ($mon_parent != $id_parent) {
			if (!sql_intervallaire::deplacer_element('spip_gens', intval($id_gens), $id_parent)) {
				return false; // quel retour ici ?
			}
		}
	} else {
		// insertion
		if (!$id_gens = sql_intervallaire::inserer_element('spip_gens', $id_parent)) {
			return array(
				'editable'=>true,
				'message_erreur'=>'Problème sur l\'insertion...'
			); 
		}
	}	

	return formulaires_editer_objet_traiter('gens', $id_gens, '', '', $retour, '');
}



?>
