<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

function action_editer_gens_dist($id_gens) {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	if (!$id_gens = intval($id_gens)) {
		$id_gens = intval($arg);
	}

	// pas de gens ? on en cree un nouveau, mais seulement si 'oui' en argument.
	if (!$id_gens) {
		if ($arg != 'oui') {
			include_spip('inc/headers');
			redirige_url_ecrire();
		}
		$id_gens = insert_gens();
	}

	if ($id_gens) $err = revisions_gens($id_gens);
	return array($id_gens, $err);
}


function insert_gens() {
	$champs = array(
		'nom' => _T('intervallaire:item_nouveau_gens')
	);
	
	// Envoyer aux plugins
	$champs = pipeline('pre_insertion', array(
		'args' => array(
			'table' => 'spip_gens',
		),
		'data' => $champs
	));
	
	$id_gens = sql_insertq("spip_gens", $champs);
	return $id_gens;
}


// Enregistrer certaines modifications d'un gens
function revisions_gens($id_gens, $c=false) {

	// recuperer les champs dans POST s'ils ne sont pas transmis
	if ($c === false) {
		$c = array();
		foreach (array('nom', 'infos') as $champ) {
			if (($a = _request($champ)) !== null) {
				$c[$champ] = $a;
			}
		}
	}
	
	include_spip('inc/modifier');
	modifier_contenu('gens', $id_gens, array(
			'nonvide' => array('nom' => _T('info_sans_titre')),
			'invalideur' => "id='id_gens/$id_gens'"
		),
		$c);
}
?>
