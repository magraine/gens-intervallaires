<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

function action_actionner_gens_dist($id_gens) {
	$securiser_action = charger_fonction('securiser_action', 'inc');
	$arg = $securiser_action();

	list($action, $id_gens) = explode('/', $arg);
	
	if (!$id_gens = intval($id_gens)) {
		$id_gens = intval($arg);
	}

	// pas de gens ? on en cree un nouveau, mais seulement si 'oui' en argument.
	if (!$id_gens) {
		include_spip('inc/minipres');
		echo minipres();
		exit;
	}

	include_spip('base/sql_intervallaire');
	
	if (!autoriser('modifier', 'gens', $id_gens)) {
		include_spip('inc/minipres');
		echo minipres();
		exit;
	}
	
	if ($action == 'supprimer_famille') {
		// supprimer le bonhomme et sa famille.
		sql_intervallaire::supprimer_element('spip_gens', $id_gens, true); 
	}
	if ($action == 'supprimer') {
		// supprimer le bonhomme et sa famille.
		sql_intervallaire::supprimer_element('spip_gens', $id_gens); 
	}
}
?>
