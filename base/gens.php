<?php

function gens_declarer_tables_interfaces($interface){
	$interface['table_des_tables']['gens'] = 'gens';	
	$interface['table_des_traitements']['NOM']['gens'] = _TRAITEMENT_TYPO; // corrections de francais
	$interface['table_des_traitements']['INFOS']['gens'] = _TRAITEMENT_RACCOURCIS; // + raccourcis spip
	return $interface;
}


function gens_declarer_tables_principales($tables_principales){
	//-- Table Gens ------------------------------------------
	$gens = array(
			"id_gens"	=> "bigint(21) NOT NULL",
			"nom"	=> "tinytext DEFAULT '' NOT NULL",
			"infos"	=> "text DEFAULT '' NOT NULL",
			"maj"	=> "TIMESTAMP",
			);
	
	$gens_key = array(
			"PRIMARY KEY"	=> "id_gens",
			);
	
	$tables_principales['spip_gens'] =
		array('field' => &$gens, 'key' => &$gens_key);

	include_spip('base/sql_intervallaire');
	$tables_principales = sql_intervallaire::declarer_champs_sql($tables_principales, 'spip_gens');
	
	return $tables_principales;
}


function gens_declarer_tables_objets_surnoms($surnoms) {
	$surnoms['gens'] = 'gens';
	return $surnoms;
}

?>
